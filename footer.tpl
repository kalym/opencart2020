    <div class="h-line">
  </div> <!-- /.main-wrap  -->
</div>
<div class="w-popup" id="info-popup">
  <div class="w-close w-action-close"><span>+</span></div>
  <div class="container-wrap"></div>
</div>
<?php
  if (!$logged) {        
      include('user_popup.php');
  }
?>
<footer>
  <div class="container">
    <div class="row">
      <?
          /*
          <!--
            <?php if ($informations) { ?>
            <div class="col-sm-3 col-xs-6">
              <h5><?php echo $text_information; ?></h5>
              <ul class="list-unstyled">
                <?php foreach ($informations as $information) { ?>
                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                <?php } ?>
              </ul>
            </div>
            <?php } ?>
          -->
          */
      ?>    
      <div class="col-sm-3 col-xs-6">
        <h5><?=$text_shop;?></h5>
        <ul class="list-unstyled">
            <?php
              if ($categories && count($categories) > 0){          
                foreach ($categories as $cat) {
                    ?><li><a href="<?=$cat['href'];?>"><?=$cat['name'];?></a></li><?
                }
              }    
            ?>       
        </ul>
      </div>

      <div class="col-sm-3 col-xs-6">
        <h5><?=$text_brend;?></h5>
        <ul class="list-unstyled">
          <li><a href="/about_us"><?=$text_about_us;?></a></li>
          <li><a href="/posts"><?=$text_blog;?></a></li>
          <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
          <li><a href="/join_the_club"><?=$text_join_us;?></a></li>
        </ul>
      </div>

      <div class="clearfix visible-xs"></div>
      
      <div class="col-sm-3 col-xs-6">
        <h5><?=$text_useful;?></h5>
        <ul class="list-unstyled">
          <li><a href="/size_grid"><?=$text_size_grid;?></a></li>
          <li><a href="/delivery_payment"><?=$text_delivery_payment;?></a></li>
          <li><a href="/faq"><?=$text_faq;?></a></li>
        </ul>
      </div>

      <div class="col-sm-3 col-xs-6">
        <h5><?=$text_corporate;?></h5>
          <ul class="list-unstyled">
            <li><a href="/partners"><?=$text_partners;?></a></li>
            <!--<li><a href="/portfolio"><?=$text_portfolio;?></a></li>-->
            <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
        </ul>
      </div>
    </div>   
  </div>      
    <div class="logo">
      <div class="wrap">
        <?
          if (basename($_SERVER['REQUEST_URI']) === '') {
              ?>           
                  <a href="http://white.com.ua/" target="_blank" class="pull-right" title="White Studio" title="White Studio"><div class="txt"><?=$text_designed_by;?></div><img    src="image/white_logo.png" width="43" height="116" alt="White Studio" /></a>
                <?
          }else{
              ?><div class="txt"><?=$text_designed_by;?></div><img src="image/white_logo.png" width="43" class="pull-right" height="116" alt="White Studio" /><?
          }          
        ?>
      </div>
    </div>
    <!-- <div class="clearfix"></div> -->
    <div class="container">
        <div class="modern-hr">
            <div class="hr-m"></div>
              <div class="img-c">            
                  <img src="image/logo_footer.png" width="72" height="85" alt="Clan-P" />
              </div>               
        </div>

        <div class="row">
          <div class="col-sm-5 text-left"  style="margin-top : -20px;">
                 <form id="email-subscribe" action="/" class="hidden-xs">
                    <div id="email-subscribe" class="input-group">                  
                        <input type="text" name="email" class="transparent" placeholder="<?=$text_subscribe_discount;?>" required/>
                        <span class="input-group-addon"><input type="submit" class="btn pull-left" value="<?=$text_join;?>"  /></span>
                    </div>              
                </form>
                
                <div class="visible-xs">
                  <br/><br/>
                </div>


                <div class="hidden-xxs">
                  <img src="image/we_accept.png" alt="Visa, Master card, PayPal, Privat 24" width="196"  height="20" /><br/><br/>
                </div>
                <div class="text-left">
                  <p class="copyright"><?php echo $powered; ?></p>
                </div>
          </div>
          <div class="col-sm-2 hidden-xs "></div>
          <div class="col-sm-5 col-xs-12 text-right" style="margin-top : -20px; padding-right: 20px;">
          </div>
        </div>        
    </div>    
</footer>
</body></html>
<?
  // print_r($this);
?>