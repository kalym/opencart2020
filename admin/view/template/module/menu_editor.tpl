<?php

echo $header; ?><?php echo $column_left; ?>
<div id="content">
<div class="page-header">
    <div class="container-fluid">
        <div class="pull-right">
            <button type="submit" form="form-menu_editor" data-toggle="tooltip" title="<?php echo $button_save; ?>"
                    class="btn btn-primary"><i class="fa fa-save"></i></button>
            <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
               class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <h1><?php echo $heading_title; ?></h1>
        <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</div>
<div class="container-fluid">
    <ul class="nav nav-tabs">
        <?php  $menuItemCount = 0; ?>
        <?php  foreach ($menu_editor_entries as $menu_key => $menu_editor_entry) { ?>

        <li <?php  if($menuItemCount==0) echo 'class="active"' ?> ><a href="#tab-<?php echo $menu_key; ?>" data-toggle="tab"><?php echo $menu_key; ?></a></li>
        <?php $menuItemCount ++; } ?>
    </ul>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data"
          id="form-menu_editor" class="form-horizontal">
    <div class="tab-content">
        <?php  $tabCount = 0; ?>
        <?php  foreach ($menu_editor_entries as $menu_key => $menu_editor_entry) { ?>
        <div class="tab-pane <?php  if($tabCount==0) echo 'active' ?>" id="tab-<?php echo $menu_key; ?>">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
                </div>
                <div id="<?php echo $menu_key; ?>-panel-body" class="panel-body">
                        <!-- STATUS -->
                        <?php $row_count=0; foreach ($menu_editor_entry as  $menu_entry) { ?>
                        <div class="form-group">

                            <label class="col-sm-1 control-label"
                                   for="menu_editor_entries_first-menu_<?php echo $row_count; ?>_names_<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>

                            <div class="col-sm-4" id="name-<?php echo $row_count; ?>" >
                                <?php foreach ($languages as $language) { ?>
                                <div class="input-group pull-left"
                                     >
                                <span class="input-group-addon">
                                    <img src="view/image/flags/<?php echo $language['image']; ?>"
                                         title="<?php echo $language['name']; ?>"/> </span>
                                    <input type="text"
                                           id="menu_editor_entries_<?php echo $menu_key; ?>_<?php echo $row_count; ?>_names_<?php echo $language['language_id']; ?>"
                                           name="menu_editor_entries[<?php echo $menu_key; ?>][<?php echo $row_count; ?>][names][<?php echo $language['language_id']; ?>]"
                                           class="form-control name-<?php echo $language['language_id']; ?>"
                                           value="<?php if (isset($menu_entry['names'][$language['language_id']])){echo $menu_entry['names'][$language['language_id']];} ?>">
                                </div>


                                <?php } ?>
                                <?php if (isset($error_entries[$row_count])) { ?>
                                <div class="text-danger"><?php echo $error_empty_line; ?></div>

                                <?php } ?>
                            </div>

                            <label class="col-sm-1 control-label" for="<?php echo $menu_key; ?>-input-href-<?php echo $row_count; ?>"><?php echo $entry_href; ?></label>

                            <div class="col-sm-4">
                                <input name="menu_editor_entries[<?php echo $menu_key; ?>][<?php echo $row_count; ?>][href]"
                                       id="<?php echo $menu_key; ?>_input-href-<?php echo $row_count; ?>" class="form-control"
                                       value="<?php echo $menu_entry['href']; ?>">
                            </div>



                            <div class="col-sm-1  text-right">
                                <button  type="button"
                                        onclick="removeRow(this);return false;"
                                        class="btn btn-danger pull-right"><i class="fa fa-trash"></i></button>
                            </div>
                        </div>
                        <?php $row_count++; } ?>

                </div>
                <div class="col-sm-1 col-sm-offset-10" >
                    <button type="button" onclick="addRow('<?php echo $menu_key; ?>')" class="btn btn-primary pull-right"><i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>

        </div>
        <?php  $tabCount ++; ?>
        <?php } ?>

    </div>
    </form>

</div>
<script type="text/javascript">

    row_count = <?php echo $row_count; ?>;
    function addRow(menuName) {
        var html = "";
        html += '<div class="form-group">';




        html += '<label class="col-sm-1 control-label" style="margin-top: 10px;" for="menu_editor_entries_'+ menuName +'-'+ row_count + '_names_<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>';
        html += '<div class="col-sm-4" id="name-' + row_count + '">';
    <?php foreach($languages as $language ){ ?>
        html += '<div class="input-group pull-left"  id="name-' + row_count + '">';
        html += '<span class="input-group-addon">';
        html += '<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> </span>';
        html += '<input id="menu_editor_entries_'+ menuName +'_'+ row_count + '_names_<?php echo $language['language_id']; ?>" type="text" "' +
            ' name="menu_editor_entries['+menuName+'][' + row_count + '][names][<?php echo $language['language_id']; ?>]" class="form-control" value="">';
        html += '</div>';

        <?php } ?>

        html += '</div>';
        html += '<label class="col-sm-1 control-label" for="'+menuName+'-input-href-' + row_count + '"><?php echo $entry_href; ?></label>';
        html += '<div class="col-sm-4">';
        html += '<input name="menu_editor_entries['+menuName+'][' + row_count + '][href]" id="'+menuName+'-input-href-' + row_count + '" class="form-control input-href" value="">';
        html += '</div>';
        html += '<div class="col-sm-1 text-right"><button  type="button"  onclick="removeRow(this)" class="btn btn-danger pull-right"><i class="fa fa-trash"></i></button></div>';
        html += '</div>';

        $("#"+menuName+"-panel-body").append(html);
        row_count++;

    }

    function removeRow(that) {
        $(that).closest(".form-group").remove();
    }


</script>
</div>
<?php echo $footer; ?>