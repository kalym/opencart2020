<?php
// HTTP
define('HTTP_SERVER', 'http://localhost:8888/vright/admin/');
define('HTTP_CATALOG', 'http://localhost:8888/vright/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost:8888/vright/admin/');
define('HTTPS_CATALOG', 'http://localhost:8888/vright/');

// DIR
define('DIR_APPLICATION', '/Applications/MAMP/htdocs/vright/admin/');
define('DIR_SYSTEM', '/Applications/MAMP/htdocs/vright/system/');
define('DIR_LANGUAGE', '/Applications/MAMP/htdocs/vright/admin/language/');
define('DIR_TEMPLATE', '/Applications/MAMP/htdocs/vright/admin/view/template/');
define('DIR_CONFIG', '/Applications/MAMP/htdocs/vright/system/config/');
define('DIR_IMAGE', '/Applications/MAMP/htdocs/vright/image/');
define('DIR_CACHE', '/Applications/MAMP/htdocs/vright/system/cache/');
define('DIR_DOWNLOAD', '/Applications/MAMP/htdocs/vright/system/download/');
define('DIR_UPLOAD', '/Applications/MAMP/htdocs/vright/system/upload/');
define('DIR_LOGS', '/Applications/MAMP/htdocs/vright/system/logs/');
define('DIR_MODIFICATION', '/Applications/MAMP/htdocs/vright/system/modification/');
define('DIR_CATALOG', '/Applications/MAMP/htdocs/vright/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'opencart2020');
define('DB_PREFIX', 'oc_');
