<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');
		
		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');


		$data['text_designed_by'] = $this->language->get('text_designed_by');

		
		/* ADDITION BY VLAD :: */
		$data['text_posts'] = $this->language->get('text_posts');


		$data['text_brend'] = $this->language->get('text_brend');
		$data['text_about_us'] = $this->language->get('text_about_us');
		$data['text_blue_label'] = $this->language->get('text_blue_label');
		$data['text_blog'] = $this->language->get('text_blog');
		$data['text_join_us'] = $this->language->get('text_join_us');
		$data['text_useful'] = $this->language->get('text_useful');
		$data['text_size_grid'] = $this->language->get('text_size_grid');
		$data['text_delivery_payment'] = $this->language->get('text_delivery_payment');
		$data['text_faq'] = $this->language->get('text_faq');
		$data['text_corporate'] = $this->language->get('text_corporate');
		$data['text_partners'] = $this->language->get('text_partners');
		$data['text_portfolio'] = $this->language->get('text_portfolio');
		$data['text_shop']  = $this->language->get('text_shop');


		$data['text_login_via'] = $this->language->get('text_login_via');//Войти через :
		$data['text_facebook'] = $this->language->get('text_facebook');//facebook
		$data['text_vkontakte'] = $this->language->get('text_vkontakte');//вконтакте
		$data['text_registration_by_email'] = $this->language->get('text_registration_by_email');//Регистрация по email
		$data['text_login_via_email'] = $this->language->get('text_login_via_email');//Регистрация по email
		$data['text_login_via_email'] = $this->language->get('text_login_via_email');//Войти через Email:
		$data['text_forgot_your_password'] = $this->language->get('text_forgot_your_password');//Забыли пароль?
		$data['text_login'] = $this->language->get('text_login');//Войти
		
		$data['text_join'] = $this->language->get('text_join');
		$data['text_subscribe_discount'] = $this->language->get('text_subscribe_discount');

        //menu_editor code begins//
        $data['menu_editor_entries'] = $this->config->get('menu_editor_entries');
        $data['language_id'] = (int)$this->config->get('config_language_id') ;
        //menu_editor code ends//
		

		/* ADDITION BY VLAD  */
		$this->load->model('catalog/information');
		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'])
				);
			}
		}
		
		// List of categories 
		$data['categories'] = array();

		// Load shop catalog 
		$this->load->model('catalog/category');

		$categories = $this->model_catalog_category->getCategories(0);
		
		foreach ($categories as $category) {
			// Skeep some categories 
			if ($category['top']==0) continue;

			$data['categories'][] = array(
				'category_id' => $category['category_id'],
				'name'        => $category['name'],
				'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
			);
		}

		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', 'SSL');
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['voucher'] = $this->url->link('account/voucher', '', 'SSL');
		$data['affiliate'] = $this->url->link('affiliate/account', '', 'SSL');
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', 'SSL');
		$data['order'] = $this->url->link('account/order', '', 'SSL');
		$data['wishlist'] = $this->url->link('account/wishlist', '', 'SSL');
		$data['newsletter'] = $this->url->link('account/newsletter', '', 'SSL');
		
		$data['posts'] 	= $this->url->link('information/posts');
		$data['logged'] = $this->customer->isLogged();

		// By VLAD 
		$data['telephone'] = 'Тел. : '.$this->config->get('config_telephone');
		$data['address']   = $this->config->get('config_address');


		$data['powered'] = '&copy; '.date('Y', time()).' '.$this->config->get('config_name').' All rights reserved';


		// sprintf($this->language->get('text_powered'), , );

		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			$this->model_tool_online->whosonline($ip, $this->customer->getId(), $url, $referer);
		}

		// Facebook Addition ... login 
		if(!$this->customer->isLogged()){
			//  User not loged in ??? 
			if(!isset($this->fbconnect)){			
				require_once(DIR_SYSTEM . 'vendor/facebook-sdk/facebook.php');
				$this->fbconnect = new Facebook(array(
					'appId'  => $this->config->get('fbconnect_apikey'),
					'secret' => $this->config->get('fbconnect_apisecret'),
				));
			}
			$data['fbconnect_url'] = $this->fbconnect->getLoginUrl(
				array(
					'scope' => 'email,user_birthday',
					 /* 'scope' => 'email,first_name,last_name,verified',*user_birthday,user_location,user_hometown*/
					'redirect_uri'  => $this->url->link('account/fbconnect', '', 'SSL')
				)
			);
			// Auth Vkontakte
			$fbconnect_vkapikey = $this->config->get('fbconnect_vkapikey');
			// redirect Vk Url
			$redirect_uri  = $this->url->link('account/vkconnect', '', 'SSL');
			//echo ' $redirect_uri : '.$redirect_uri;

			$fbconnect_vkurl  = 'http://oauth.vk.com/authorize?client_id='.$fbconnect_vkapikey.'&redirect_uri='.$redirect_uri.'&response_type=code';
			$data['fbconnect_vkurl'] = $fbconnect_vkurl;
	

			// TODO :: translation 
		}

















		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/footer.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/common/footer.tpl', $data);
		} else {
			return $this->load->view('default/template/common/footer.tpl', $data);
		}
	}
}