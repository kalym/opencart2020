<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
<link rel="icon" href="/favicon.ico" type="image/x-icon" />
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>

<script src="catalog/view/javascript/jquery/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery-browser-plugin/jquery.browser.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/imgliquid/imgliquid.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/slides/jquery.slides.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/dropkick/dropkick.2.1.4.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/mousewheel/jquery.mousewheel.min.js" type="text/javascript"></script>
<script src="catalog/view/javascript/mCustomScrollbar/jquery.mCustomScrollbar.min.js" type="text/javascript"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_VoWmApe5HZNVeRFYBY2S8X73jZOP75k"></script>

<script src="catalog/view/javascript/cloud-zoom/jquery.cloud-zoom.js" type="text/javascript"></script>
<!--
<script src="catalog/view/javascript/jqzoom/jquery.jqzoom-core.pack.js" type="text/javascript"></script>
-->
<script src="catalog/view/javascript/common.js" type="text/javascript"></script>




<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/default/stylesheet/dropkick.css" rel="stylesheet" />
<link href="catalog/view/theme/default/stylesheet/jquery-ui.min.css" rel="stylesheet" />
<link href="catalog/view/theme/default/stylesheet/jquery.mCustomScrollbar.css" rel="stylesheet" />

<link href="catalog/view/theme/default/stylesheet/cloud-zoom.css" rel="stylesheet" />
<!--
<link href="catalog/view/theme/default/stylesheet/jquery.jqzoom.css" rel="stylesheet" />
-->
<link href='http://fonts.googleapis.com/css?family=PT+Sans+Narrow:400,600' rel='stylesheet' type='text/css'>
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<link href="catalog/view/theme/default/stylesheet/stylesheet.css?t=<?php echo time();?>" rel="stylesheet" />

<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>

<?php echo $google_analytics; ?>
    <style>
        li.spacer:last-child{
            display: none;
        }
    </style>
</head>
<body class="<?php echo $class; ?>">
<div class="main-wrap">
<div id="nav-left-bg"  class="hidden-sm hidden-xs"></div>
<div id="nav-right-bg"  class="hidden-sm hidden-xs"></div>
<nav id="top" class="hidden-sm hidden-xs">
    <div class="m-container container">
        <div class="row no-padding">          
          <div class="col-md-3 title-row">
          </div>
          <div class="col-md-3 search-row">
              <?php echo $search; ?>
          </div>
          <div class="col-md-6 menu-row">
              <div id="top-links">
                    <? /* Language selection; */ ?>
                    <?php echo $language; ?>
                    <? /* Nav Menu  */ ?>
                    <ul id="top-links-list" class="list-inline">
                        <?php if ($logged) { ?>
                            <li class="username"><a href="<?php echo $account; ?>" title="<?php echo $text_login; ?>" class=""><span class="hidden-lg"><i class="fa fa-user"></i></span><span class="hidden-xs hidden-sm hidden-md"><?
                             if ($user_image != ''){
                                echo '<img src="'.$user_image.'" width="20" height="20" alt="" class="userimage"/>&nbsp;&nbsp;';
                             }
                            ?><?=$username;?></span></a>
                         </li>
                         <? } else { ?>
                            <li><a href="<?php echo $account; ?>" title="<?php echo $text_login; ?>" class="open-cart user-auth"><span class="hidden-lg"><i class="fa fa-user"></i></span><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_login; ?></span></a>
                            </li>  
                          <? } ?>                         
                          <? /*
                           <!--
                           <ul class="dropdown-menu dropdown-menu-right">
                             <?php if ($logged) { ?>
                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                 <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                <?                                  
                                    //<li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>
                                    //<li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li>                                  
                                ?>
                                <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                             <?php } else { ?>
                             <li><a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
                             <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a></li>
                             <?php } ?>
                           </ul>
                           --> */?>
                         
                         <li><a href="<?php echo $shopping_cart; ?>" title="<?php echo $text_shopping_cart; ?>" class="open-cart"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_shopping_cart; ?></span></a>
                         </li>
                         <li><a href="/delivery_payment" title=""><span class="hidden-lg"><i class="fa fa-truck"></i> </span><span class="hidden-xs hidden-sm hidden-md"><?=$text_delivery_and_payment;?></span></a></li>
                    </ul>
              </div>             
          </div>
        </div>
    </div>
</nav>
<header>
  <div class="container">
    <div class="row no-padding">
      <div class="col-xs-8 col-sm-4 col-md-4">
        <div id="logo">            
              <h1><a href="<?php echo $base; ?>"><img src="/image/logo.png" width="" height="" alt="ClanP" /> <img src="/image/logo_clanp.png" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" style="margin-top:-3px;"/></a></h1>          
        </div>
      </div>        
      <div class="col-xs-4 col-sm-8 col-md-8">
          <div class="menu-mobile visible-sm visible-xs">                                    
              <button type="button" class="btn btn-navbar navbar-toggle visible-sm visible-xs" data-toggle="collapse" 
              data-target=".navbar-ex1-collapse" data-parent="#category-menu"><i class="fa fa-bars"></i></button>
              <a href="/cart" class="btn-cart pull-right"><i class="fa fa-shopping-cart "></i></a>
          </div>
          <?           
            if ($categories) {
              ?>
                <ul class="list-inline pull-right menu hidden-xs hidden-sm" >
                    <?php  foreach ($menu_editor_entries['header-menu'] as $menu_key => $menu_editor_entry) { ?>
                    <li><a href="<?php echo $menu_editor_entry['href'] ?>"><?php echo $menu_editor_entry['names'][$language_id] ?></a></li><li class='spacer'>|</li>
                    <?php } ?>
                </ul>
              <?
            }
        ?>
          <div class="hidden-xs hidden-sm">
             <div class="clearfix"></div>
              <ul class="list-inline pull-right menu menu-addition">
                  <?php  foreach ($menu_editor_entries['header-submenu'] as $menu_key => $menu_editor_entry) { ?>
                  <li><a href="<?php echo $menu_editor_entry['href'] ?>"><?php echo $menu_editor_entry['names'][$language_id] ?></a></li><li class='spacer'>|</li>
                  <?php } ?>
              </ul>
          </div>
      </div>  

      <!-- hidden user menu -->
      <div class="col-xs-12 col-sm-12 visible-sm visible-xs">
          <div class="collapse navbar-ex1-collapse">
              <ul class="nav" id="mobile-category">                                                        
                  <?                      
                      foreach ($categories as $category) {                      
                        ?><li><a href="<?php echo $category['href']; ?>" class="<?php echo $category['status']; ?>"><?php echo $category['name']; ?></a></li><?
                      }
                  ?>
                  <li class="devider"></li>
                  <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                  <li><a href="/delivery_payment" title=""><?=$text_delivery_and_payment;?></a></li>

                  <li class="devider"></li>
                  <li><a href="<?=$manufacturer;?>"><?=$text_designers;?></a></li>
                  <li><a href="/posts"><?=$text_posts;?></a></li>                  
                  <li><a href="/about_us"><?=$text_about_us;?></a></li>
                  <li><a href="/contact"><?=$text_contacts;?></a></li>
              </ul>
          </div>  
      </div>

    </div>
  </div>
</header>
<div class="container header-ending">
  <div class="row">
    <div class="col-xs-12"><hr class="double" />    </div>
  </div>
</div>
<div class="w-popup" id="cart-popup"></div>
<? 
  // Popup cart by vlad
    // echo $cart;
?>
<?php
	echo $callbackrequest;
?>